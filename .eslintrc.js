module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended',
    'airbnb-base'
  ],
  plugins: [
  ],
  // add your custom rules here
    rules: {
      'nuxt/no-cjs-in-config': 'off',
      'class-methods-use-this': 0,
      'linebreak-style': 0,
      'arrow-body-style': 'off',
      'consistent-return': 'off',
      'import/extensions': 'off',
      'import/no-extraneous-dependencies': 'off',
      'import/no-unresolved': 'off',
      'max-len': 'off',
      'no-param-reassign': 'off',
      'no-plusplus': 'off',
      'no-underscore-dangle': 'off',
      'no-useless-escape': 'off',
      'no-console': 'off',
      'radix': 'off',
      'indent': ['error', 2]
  }
}
